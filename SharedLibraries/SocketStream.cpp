#include "stdafx.h"
#include "SocketStream.h"
#include <string.h>
#ifndef min
#define min(X,Y) ((X)<(Y)?(X):(Y))
#endif

#ifdef WIN32
#pragma comment(lib, "Ws2_32.lib")
unsigned SocketStream::mInstancesCreated = 0;
WSADATA SocketStream::mWSAData;
#endif

const char SocketStream::HTTPRequestHeader[]=
	"POST / HTTP/1.1\r\n"
	"Host: www.farsnews.com\r\n"
	"User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5 (.NET CLR 3.5.30729)\r\n"
	"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n"
	"Accept-Language: en-us,en;q=0.5\r\n"
	"Accept-Encoding: gzip,deflate\r\n"
	"Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\n"
	"Keep-Alive: 300\r\n"
	"Connection: keep-alive\r\n"
	"Content-Type: application/octet-stream\r\n"
	"Content-Length:";
const char SocketStream::HTTPResponseHeader[]=
	"HTTP/1.1 200 OK\r\n"
	"Server: Apache/1.3.3.7 (Unix) (Red-Hat/Linux)\r\n"
	"Content-Type: application/octet-stream\r\n"
	"Content-Length:";
const unsigned int SocketStream::HTTPRequestHeaderLength = static_cast<unsigned int>(strlen(SocketStream::HTTPRequestHeader));
const unsigned int SocketStream::HTTPResponseHeaderLength = static_cast<unsigned int>(strlen(SocketStream::HTTPResponseHeader));

SocketStream::SocketContainer::SocketContainer () : value(-1), uses(nullptr)
{
}

SocketStream::SocketContainer::SocketContainer(SOCKET s): value(s>=0?s:-1), uses(s>=0?new std::atomic<int>(1):nullptr)
{
}

SocketStream::SocketContainer::SocketContainer(const SocketContainer&other): value(other.value), uses(other.uses) 
{
	if(uses)
		++*uses;
}

SocketStream::SocketContainer::SocketContainer(SocketStream::SocketContainer&&other): value(other.value), uses(other.uses) 
{
	other.uses = nullptr; 
	other.value = -1;
}

SocketStream::SocketContainer& SocketStream::SocketContainer::operator = (SOCKET s)
{
	if (s<0)s=-1;
	if(s == value)
		return *this;
	close(s>=0?new std::atomic<int>(1):nullptr);
	value=s;
	return *this;
}

SocketStream::SocketContainer& SocketStream::SocketContainer::operator = (SocketContainer s)
{
	if(s.value == value)
		return *this;
	close(s.uses);
	value=s.value;
	if(uses)
		++*uses;
	return *this;
}

SocketStream::SocketContainer::operator SOCKET () 
{
	return value;
}

void SocketStream::SocketContainer::close(std::atomic<int>* nextUses)
{
	if (uses && --*uses == 0)
	{
		delete uses; 
#ifdef WIN32
		::closesocket(value);
#else
		::close(value);
#endif
	}
	uses = nextUses;
}
SocketStream::SocketContainer::~SocketContainer()
{
	close();
}

SocketStream::SocketStream() : HTTPReceiveBuffer(new char[1000])
{
#ifdef WIN32
	if (mInstancesCreated == 0) 
		WSAStartup(MAKEWORD(2,0), &mWSAData); 
	mInstancesCreated ++; 
#endif
	mBlockMode = BM_NEEDED;
	mProtocol = PM_NONE;
	mReceiveQueue = 0;
	mConnected = CM_Disconnected;
	mSocket = -1;
}

SocketStream::SocketStream(SOCKET pSocket) : mSocket (pSocket), HTTPReceiveBuffer(new char[1000])
{
#ifdef WIN32
	if (mInstancesCreated == 0) 
		WSAStartup(MAKEWORD(2,0), &mWSAData); 
	mInstancesCreated ++; 
#endif
	mBlockMode = BM_NEEDED;
	mProtocol = PM_NONE;
	mReceiveQueue = 0;
	if (pSocket >= 0)
		mConnected = CM_Both;
	else
	{
		mConnected = CM_Disconnected;
		mSocket = -1;
	}
}

SocketStream::SocketStream(const SocketStream &pStream): HTTPReceiveBuffer(new char[1000])
{
#ifdef WIN32
	mInstancesCreated ++; 
#endif
	mSocket = pStream.mSocket;
	mBlockMode = pStream.mBlockMode;
	mConnected = pStream.mConnected;
	mProtocol = pStream.mProtocol;
	mReceiveQueue = 0;
}

SocketStream::~SocketStream()
{
	delete []HTTPReceiveBuffer;
#ifdef WIN32
	mInstancesCreated --; // Decrease The Number of instances Created from this class
	if (mInstancesCreated == 0) // Check if this class is the last class using Socket
		WSACleanup(); // Clean up dll loaded Files
#endif
}

void SocketStream::initialize()
{
	mConnected = CM_Disconnected;
	mBlockMode = BM_NEEDED;
	mSocket = socket( AF_INET, // Creates the Socket , The Internet Protocol version 4 (IPv4) address family.
		SOCK_STREAM,  // Provides sequenced, reliable, two-way, connection-based byte streams 
		IPPROTO_TCP ); // The Transmission Control Protocol (TCP).
	mProtocol = PM_NONE;
#ifdef WIN32
	if (mSocket == INVALID_SOCKET) // Invalid Socket
		_ASSERT(mSocket != INVALID_SOCKET && "Socket Creation Failed.");
#endif
}

int SocketStream::send(const char* data, unsigned length)
{	
	if (!isConnected(CM_Send))
		return 0;
	if (((mProtocol & PM_DISGUISE_CLIENT) == PM_DISGUISE_CLIENT || (mProtocol & PM_DISGUISE_SERVER) == PM_DISGUISE_SERVER) &&  
		mReceiveQueue == 0)
	{
		ProtocolMode temp = mProtocol;		
		if ((mProtocol & PM_DISGUISE_CLIENT) == PM_DISGUISE_CLIENT)
		{
			mProtocol = PM_NONE;
			send(HTTPRequestHeader,HTTPRequestHeaderLength);
		}
		if ((mProtocol & PM_DISGUISE_SERVER) == PM_DISGUISE_SERVER)
		{
			mProtocol = PM_NONE;
			send(HTTPResponseHeader,HTTPResponseHeaderLength);
		}
		(*this) << length << "\r\n\r\n";
		mProtocol = temp;
	}
	int temp;
	unsigned bytesToSend = length;
	if ((mProtocol & PM_Header) && (mProtocol & (PM_DISGUISE_CLIENT && PM_DISGUISE_SERVER)) == 0)
	{
		std::swap (((char*)&bytesToSend)[0],((char*)&bytesToSend)[3]);
		std::swap (((char*)&bytesToSend)[1],((char*)&bytesToSend)[2]);
		temp = ::send(mSocket, (char*)&bytesToSend, 4, 0); // Send packet header which indicates how many bytes are we going to send ( 4 bytes doesn't split!!!)
		std::swap (((char*)&bytesToSend)[0],((char*)&bytesToSend)[3]);
		std::swap (((char*)&bytesToSend)[1],((char*)&bytesToSend)[2]);		
	}
	else
		temp = 4;
	if (temp <=0)
	{
		disconnect(CM_Send);
		return 0;
	}		
	unsigned bytesSent = 0;
	if (mProtocol & PM_Encrypted)
		for(unsigned i=0;i<bytesToSend;i++)
			const_cast<char*>(data)[i] ++;
	while (bytesSent < bytesToSend)
	{
		temp = ::send(mSocket, data + bytesSent, bytesToSend - bytesSent, 0); 
        int error =0;
#ifdef WIN32
		error = WSAGetLastError();
#endif
		if (temp <= 0)
		{
			disconnect(CM_Send);
			return bytesSent;
		}			
		bytesSent += temp;	
		if (mBlockMode != BM_ALWAYS && (mBlockMode != BM_NEEDED || (mProtocol & PM_Header) == 0))
			break;
				
	}
	if (mProtocol & PM_Encrypted)
		for(unsigned i=0;i<bytesToSend;i++)
			const_cast<char*>(data)[i] --;	
	return bytesSent;		
}

int SocketStream::receive(char* data, unsigned length)
{
	if (!isConnected(CM_Receive))
		return 0;
	if (((mProtocol & PM_DISGUISE_CLIENT) == PM_DISGUISE_CLIENT || (mProtocol & PM_DISGUISE_SERVER) == PM_DISGUISE_SERVER) &&  
		mReceiveQueue == 0)
	{
		ProtocolMode temp = mProtocol;		
		BlockMode tempBlock = mBlockMode;
		unsigned int headerLength = 0;
		unsigned int estimatedpacketlength;

		if ((mProtocol & PM_DISGUISE_CLIENT) == PM_DISGUISE_CLIENT)
		{
			estimatedpacketlength = HTTPResponseHeaderLength + 4 + 1;
		}

		if ((mProtocol & PM_DISGUISE_SERVER) == PM_DISGUISE_SERVER)
		{
			estimatedpacketlength = HTTPRequestHeaderLength + 4 + 1;
			
		}
		mProtocol = PM_NONE;	
		mBlockMode = BM_ALWAYS;
		headerLength = receive(HTTPReceiveBuffer,estimatedpacketlength);		
		if (!isConnected(CM_Receive))
		{				
			disconnect(CM_Receive);
			return 0;
		}
		else
			while (true)
			{
				if (HTTPReceiveBuffer[headerLength - 4] != '\r')
					estimatedpacketlength ++;
				if (HTTPReceiveBuffer[headerLength - 3] != '\n' && HTTPReceiveBuffer[headerLength - 3] != '\r')
					estimatedpacketlength ++;
				if (HTTPReceiveBuffer[headerLength - 2] != '\n' && HTTPReceiveBuffer[headerLength - 2] != '\r')
					estimatedpacketlength ++;
				if (HTTPReceiveBuffer[headerLength - 1] != '\n' && HTTPReceiveBuffer[headerLength - 1] != '\r')
					estimatedpacketlength ++;
				if (headerLength != estimatedpacketlength)
					if (estimatedpacketlength < 1000)
						headerLength += receive(HTTPReceiveBuffer+headerLength,estimatedpacketlength - headerLength);
					else
						disconnect(CM_Receive);
				else
					break;
				if (!isConnected(CM_Receive))
					return 0;
			}
		HTTPReceiveBuffer[headerLength - 4] = 0;
		mProtocol = temp;
		if ((mProtocol & PM_DISGUISE_CLIENT) == PM_DISGUISE_CLIENT)
		{
			mReceiveQueue = atoi(HTTPReceiveBuffer + HTTPResponseHeaderLength);
		}

		if ((mProtocol & PM_DISGUISE_SERVER) == PM_DISGUISE_SERVER)
		{
			mReceiveQueue = atoi(HTTPReceiveBuffer + HTTPRequestHeaderLength);			
		}
		mBlockMode = tempBlock;
	}
	int temp;

	if (mProtocol & PM_Header)
	{				
		if (mReceiveQueue == 0 && (mProtocol & (PM_DISGUISE_CLIENT && PM_DISGUISE_SERVER)) == 0)
		{
			temp = recv(mSocket, (char*)&mReceiveQueue, 4, MSG_WAITALL);
			if (temp < 0)
			{
				disconnect(CM_Receive);
				return 0;
			}		
			std::swap (((char*)&mReceiveQueue)[0],((char*)&mReceiveQueue)[3]);
			std::swap (((char*)&mReceiveQueue)[1],((char*)&mReceiveQueue)[2]);		
		}
		length = min(length,mReceiveQueue);
	}

	unsigned bytesreceived = 0;
	while (bytesreceived < length)
	{
		temp = ::recv(mSocket, data + bytesreceived, length - bytesreceived, 0); 
        int error =0;
#ifdef WIN32
		error = WSAGetLastError();
#endif
		if (temp <= 0)
		{
			disconnect();
			return bytesreceived;
		}
		bytesreceived += temp;
		if (mProtocol & PM_Header)
			mReceiveQueue -= temp;
		if (mBlockMode != BM_ALWAYS && (mBlockMode != BM_NEEDED || (mProtocol & PM_Header) == 0))
			break;
	}
	if (mProtocol & PM_Encrypted)
		for(unsigned i=0;i < bytesreceived; i++)
			const_cast<char*>(data)[i] --;
	return bytesreceived;		
}

SocketStream& SocketStream::operator >> (std::string& x)
{
	if (!isConnected(CM_Receive))
		return *this;		
	char c[1];
	x = "";
	if (mProtocol != PM_Header)
		while (x.length() == 0 || x[x.length() - 1] != '\n')
		{
			if (receive(c,1) >0)			
				x.append(c,1);			
			if (!isConnected())
				break;
		}		
	else
	{
		BlockMode mode = mBlockMode;
		mBlockMode = BM_NEVER;
		receive(c,0);
		x.resize(mReceiveQueue);
		mBlockMode = BM_ALWAYS;
		receive(const_cast<char*>(x.c_str()),static_cast<unsigned int>(x.size()));
		mBlockMode = mode;
	}

	return *this;
}

bool SocketStream::disconnect(ConnectionMode cm)
{	
	if (mSocket < 0)
	{
		mConnected = CM_Disconnected;
		return false;
	}

	if ((mConnected & cm) != 0)
	{
		bool result = shutdown(mSocket, (mConnected & cm) - 1) == 0;
		mConnected = static_cast<ConnectionMode>(mConnected & ~cm);
		return result;
	}
	return false;
}

void SocketStream::closeSocket()
{
	mSocket = -1;
}

template <> SocketStream& SocketStream::operator <<(const std::string& x)
{
	if (!isConnected(CM_Send))
		return *this;
	send(x.c_str(), static_cast<unsigned int>(x.length()));
	return *this;
}