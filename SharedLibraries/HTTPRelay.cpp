#include "stdafx.h"
#include "HTTPRelay.h"
#include <thread>
#include <chrono>
#include <fstream>
#include <sstream>
#include <memory.h>
#include <string.h>
#include "ClientSocketStream.h"

using namespace std;

#ifndef min
#define min(X,Y) ((X)<(Y)?(X):(Y))
#endif


#if LOGLEVEL > 0
std::string HTTPRelay::appname;
int HTTPRelay::conID;
#endif

HTTPRelay::HTTPRelay(SocketStream &A, SocketStream &B, int blockSize, int queueSize, ProxyMode mode)
{	
	if (mode == PM_http)
	{
		HTTPRelayWorker* worker (new HTTPRelayWorker(A,B,blockSize,queueSize,PM_http_request));				
		worker->mPartner = nullptr;
#if LOGLEVEL > 0
		worker->setWorkerID(conID * 2);	
#endif
		worker->mReceiverThread = new thread([worker](){worker->backgroundReceiver();});	
		thread([worker](){worker->backgroundSender();}).detach();			
	}
	else
	{
		HTTPRelayWorker* workerA (new HTTPRelayWorker(A,B,blockSize,queueSize,mode));		
		HTTPRelayWorker* workerB (new HTTPRelayWorker(B,A,blockSize,queueSize,PM_https));	
		workerA->mPartner = workerB;
		workerB->mPartner = workerA;
#if LOGLEVEL > 0
		workerA->setWorkerID(conID * 2);
		workerB->setWorkerID(conID * 2 + 1);
#endif
		workerA->mReceiverThread = new thread([workerA](){workerA->backgroundReceiver();});	
		thread([workerA](){workerA->backgroundSender();}).detach();	
		workerB->mReceiverThread = new thread([workerB](){workerB->backgroundReceiver();});	
		thread([workerB](){workerB->backgroundSender();}).detach();			
	}
	
}

HTTPRelay::HTTPRelayWorker::HTTPRelayWorker(SocketStream &in, SocketStream &out, int blockSize, int queueSize, ProxyMode mode) : 
	mIn(in), 
	mOut(out), 
	mBlockSize(blockSize), 
	mQueueSize(queueSize),
	mProxyMode(mode),
	mReceiverThread(nullptr),
	mTransferEncoding(TE_Normal)
{
	mSharedBuffer = new char[(mBlockSize + 4) * mQueueSize];
	mQBegin = mQEnd = 0;	
	mQueueCondition = QC_Empty;	
}

HTTPRelay::HTTPRelayWorker::~HTTPRelayWorker()
{	
	mIn.disconnect();
	if (mPartner)
		mPartner->mPartner = nullptr;
	mBufferTransferLock.unlock();
	mQConditionFullLock.lock();
	mQConditionFullLock.unlock();
	mBufferFullVariable.notify_all();
	mReceiverThread->join();
	delete []mSharedBuffer;
	if (mPartner == nullptr || mPartner->mProxyMode == PM_https)
		mOut.disconnect();
	delete mReceiverThread;
	mQConditionEmptyLock.unlock();
		
				
#if LOGLEVEL >= LOG_PACKAGES
	if (LOGIn.is_open())	
		LOGIn.close();			
	if (LOGOut.is_open())
		LOGOut.close();
#endif
}

#if LOGLEVEL > 0
void HTTPRelay::HTTPRelayWorker::setWorkerID(int ID)
{
	mWorkerID = ID;
#if LOGLEVEL >= LOG_PACKAGES
#ifdef WIN32
	LARGE_INTEGER t1, t2, p1, p2, f;
	unsigned long long r1,r2;

	QueryPerformanceFrequency(&f);
	QueryPerformanceCounter(&p1);
	r1 = __rdtsc();
	
	Sleep(1);
	GetSystemTimeAsFileTime((FILETIME*) (&t1.u));
	do
	{
		Sleep(0);
		GetSystemTimeAsFileTime((FILETIME*) (&t2.u));		
	} while(t2.QuadPart == t1.QuadPart);

	QueryPerformanceCounter(&p2);
	r2 = __rdtsc();

	CPUPerformance = f.QuadPart * (r2 - r1) / (p2.QuadPart-p1.QuadPart);
	CPUStartClock = __rdtsc();
#endif
	char fName[50];
	if (LOGIn.is_open())	
		LOGIn.close();
	if (LOGOut.is_open())
		LOGOut.close();
	
	if (ID % 2 == 0)
	{
		sprintf(fName,"../Log/%03dABIn%s.txt",ID / 2,HTTPRelay::appname.c_str());LOGIn.open(fName,ios::binary|ios::app);	
		sprintf(fName,"../Log/%03dABOu%s.txt",ID / 2,HTTPRelay::appname.c_str());LOGOut.open(fName,ios::binary|ios::app);
	}
	else
	{
		sprintf(fName,"../Log/%03dBAIn%s.txt",ID / 2,HTTPRelay::appname.c_str());LOGIn.open(fName,ios::binary|ios::app);	
		sprintf(fName,"../Log/%03dBAOu%s.txt",ID / 2,HTTPRelay::appname.c_str());LOGOut.open(fName,ios::binary|ios::app);
	}
#endif
}

template <> void HTTPRelay::HTTPRelayWorker::writeLog(LOGType type, const char* data)
{
	writeLog(type, data,strlen(data));
}


void HTTPRelay::HTTPRelayWorker::writeLog(LOGType type, const char* data, int length)
{
	ostream* temp = nullptr;
	switch (type)
	{
	case HTTPRelay::HTTPRelayWorker::LT_In:
		temp = &LOGIn;
		break;
	case HTTPRelay::HTTPRelayWorker::LT_Out:
		temp = &LOGOut;
		break;
	case HTTPRelay::HTTPRelayWorker::LT_Partner_In:
		if (mPartner)
			temp = &mPartner->LOGIn;
		break;
	case HTTPRelay::HTTPRelayWorker::LT_Partner_Out:
		if (mPartner)
			temp = &mPartner->LOGOut;
		break;
	default:
		break;
	}
	if (temp)
	{
		ostream& out = *temp;
#if LOGLEVEL >= LOG_PACKAGES
#if LOGLEVEL > 3
		out << "--LOG BEGIN--";
		writeTime(type);
#endif
		out.write(data,length);
		out.flush();
#if LOGLEVEL > 3
		out << "--LOG END-- ";
		writeTime(type);
#endif		
#endif
	}
}
#ifdef WIN32
void HTTPRelay::HTTPRelayWorker::writeTime(LOGType type)
{
#if LOGLEVEL >= LOG_ALL
	ostream* temp = nullptr;
	switch (type)
	{
	case HTTPRelay::HTTPRelayWorker::LT_In:
		temp = &LOGIn;
		break;
	case HTTPRelay::HTTPRelayWorker::LT_Out:
		temp = &LOGOut;
		break;
	case HTTPRelay::HTTPRelayWorker::LT_Partner_In:
		if (mPartner)
			temp = &mPartner->LOGIn;
		break;
	case HTTPRelay::HTTPRelayWorker::LT_Partner_Out:
		if (mPartner)
			temp = &mPartner->LOGOut;
		break;
	default:
		break;
	}
	if (temp)
	{
		ostream& out = *temp;

		out << "-- time = (" << ((__rdtsc() - CPUStartClock) * 1000000ULL) / CPUPerformance << "�s)\n";		
		out.flush();
	}
	#endif
}
#endif

#endif


void HTTPRelay::HTTPRelayWorker::backgroundReceiver()
{
	unique_lock<recursive_mutex> conditionLock(mQConditionFullLock);
	char* receiveBuffer;
	int* sequenceLength;
	int headerLength = 0;
	int matchedLength = 0;
	int lastMatch = 0;
	char* requestLine;
	
	while (mIn.isConnected())
	{		
		if (mProxyMode == PM_http_request || mProxyMode == PM_http_response)
		{
			finishPacketTransmission();
			hostResolved = false;
			mContentLength = 0;
			sequenceLength = 0;
			mTransferEncoding = TE_Normal;
			mQBegin = 0;
			mQEnd = 0;

			mSharedBuffer += 4 * mQueueSize;
			requestLine = mSharedBuffer;
			for(int i=0;i<headerLength - matchedLength;i++)
				mSharedBuffer[i] = mSharedBuffer[matchedLength + i];			
			headerLength -= matchedLength;
			matchedLength = 3;
			do
			{	
				if (headerLength <= matchedLength)
					headerLength += mIn.receive(mSharedBuffer + headerLength, mBlockSize * mQueueSize - headerLength);				
				if (!mIn.isConnected())
				{					
					mOut.disconnect();
					mBufferTransferLock.unlock();
#if LOGLEVEL > 0
					cout << mWorkerID << " closed unexpectedly after sending:\n";
					cout.write(mSharedBuffer, headerLength);
#endif
					mSharedBuffer -= 4 * mQueueSize;
					goto endThread;
				}
				if (headerLength < 3)
					continue;
				if (headerLength == (mBlockSize-1) * mQueueSize)
				{
					hostResolved = false;
					break;
				}
				mSharedBuffer[headerLength] = 0;
				do
				{					
					if (mSharedBuffer[matchedLength - 2] == '\n' &&
						mSharedBuffer[matchedLength - 3] == '\r')
						lastMatch = matchedLength;
					while (matchedLength < headerLength 
						&&(mSharedBuffer[matchedLength - 1] != '\n' 
						|| mSharedBuffer[matchedLength - 2] != '\r')
						&& mSharedBuffer[matchedLength - 1] != 0)
						matchedLength ++;
					if (mSharedBuffer[matchedLength - 1] != '\n' &&
						mSharedBuffer[matchedLength - 2] != '\r')
						break;					
					if(mProxyMode != PM_https)										
						if (requestLine != mSharedBuffer)					
							HTTPParseHeader(requestLine);		
						else
							HTTPParseRequest(requestLine);
					writeTime(LT_In);
					requestLine = mSharedBuffer + matchedLength++;										
				}				
				while(lastMatch + 2 != matchedLength);
				if (lastMatch + 2 == matchedLength--)
					break;				
				matchedLength++;				
			}
			while(true);			
			requestLine = mSharedBuffer;
			if ((requestLine [matchedLength-2] != '\r' || requestLine[matchedLength-1] != '\n') && mProxyMode == PM_http_request)
			{
				mIn << "HTTP/1.1 400 Bad Request (Request header too long)\r\n\r\n";
				writeLog(LT_Partner_Out, "HTTP/1.1 400 Bad Request (Request header too long)\r\n\r\n");
				continue;
			}		

			if (!hostResolved && mProxyMode == PM_http_request)
			{
				mIn << "HTTP/1.1 400 Bad Request (host not provided)\r\n\r\n";
				writeLog(LT_Partner_Out, "HTTP/1.1 400 Bad Request (host not provided)\r\n\r\n");
				continue;
			}
			if (mTransferEncoding == TE_Normal)
			{
				if (matchedLength + mContentLength <= headerLength)
				{
					matchedLength += mContentLength;
					mContentLength = 0;
				}
				else
				{
					mContentLength -= headerLength - matchedLength;
					matchedLength = headerLength;
				}
			}
			while (mTransferEncoding == TE_Chunked_header)
			{
				mContentLength = 0;
				while (matchedLength <= headerLength && (requestLine[matchedLength] >= '0' && requestLine[matchedLength] <= '9') || (((requestLine[matchedLength])&~32) >= 'A' && ((requestLine[matchedLength])&~32) <= 'F'))
				{
					mContentLength *= 16;
					mContentLength += (requestLine[matchedLength] >= '0' && requestLine[matchedLength] <= '9')? 
						requestLine[matchedLength] - '0':
						((requestLine[matchedLength])&~32) - 'A' + 10;
					matchedLength ++;
				}
				if (mContentLength != 0)
					mContentLength += 2;
				while (matchedLength <= headerLength && requestLine[matchedLength - 2] != '\r' || requestLine [matchedLength-1] != '\n')
					matchedLength ++;
				if (matchedLength <= headerLength)
					if (mContentLength > 0)
						mTransferEncoding = TE_Chunked_body;
					else
						mTransferEncoding = TE_Chunked_trailer;
				if (mTransferEncoding == TE_Chunked_body)
					if (matchedLength + mContentLength <= headerLength)
					{
						mTransferEncoding = TE_Chunked_header;
						matchedLength += mContentLength;
					}
					else
					{
						mContentLength -= headerLength - matchedLength;
						matchedLength = headerLength;
					}
			}
			writeLog(LT_In, mSharedBuffer, matchedLength);
			mSharedBuffer -= 4 * mQueueSize;
			if (mProxyMode == PM_https)
			{
				if (headerLength != matchedLength)
				{
					for(int i=0;i < headerLength / mBlockSize;i++)
						*reinterpret_cast<int*>(mSharedBuffer + i * 4) = mBlockSize;
					*reinterpret_cast<int*>(mSharedBuffer + (headerLength / mBlockSize) * 4) = headerLength % mBlockSize;
					mQEnd = ((headerLength - matchedLength) / mBlockSize + 1) % mQueueSize;
					mQueueCondition = mQEnd == 0?QC_Full:QC_Normal;
				}
				else
				{
					mQEnd = 0;
					mQueueCondition = QC_Empty;
				}
				mBufferTransferLock.unlock();
				continue;
			}			
			for(int i=0;i < matchedLength / mBlockSize;i++)
				*reinterpret_cast<int*>(mSharedBuffer + i * 4) = mBlockSize;
			*reinterpret_cast<int*>(mSharedBuffer + (matchedLength / mBlockSize) * 4) = matchedLength % mBlockSize;
			mQEnd = (matchedLength / mBlockSize + 1) % mQueueSize;			
			mQueueCondition = (mQBegin == mQEnd?QC_Full:QC_Normal);
			mBufferTransferLock.unlock();						
			mQConditionEmptyLock.lock();
			mQConditionEmptyLock.unlock();
			mBufferEmptyVariable.notify_all();

			if (mQueueCondition == QC_Full)
			{
				mBufferFullVariable.wait(conditionLock,[this]{return this->mQueueCondition != QC_Full || !mOut.isConnected();});		
				if (!mOut.isConnected())
					goto endThread;
			}
		}
		else
			mContentLength = mBlockSize;

		while (mContentLength > 0 || mTransferEncoding != TE_Normal)
		{	
			if (mTransferEncoding == TE_Chunked_header || mTransferEncoding == TE_Chunked_trailer)
				mContentLength = mBlockSize;
			receiveBuffer = mSharedBuffer + mQEnd * mBlockSize + mQueueSize * 4;
			sequenceLength = reinterpret_cast<int*>(mSharedBuffer + mQEnd * 4);
			*sequenceLength = mIn.receive(receiveBuffer, min(mBlockSize,mContentLength));
			writeLog(LT_In, receiveBuffer,*sequenceLength);
			if (mProxyMode == PM_https)
				mContentLength = 0;
			else
			{					
				requestLine = receiveBuffer;
				if (mTransferEncoding == TE_Chunked_body)
				{
					mContentLength -= *sequenceLength;
					if (mTransferEncoding == TE_Chunked_body && mContentLength == 0)
						mTransferEncoding = TE_Chunked_header;
				}
				else
				{
					mContentLength = 0;
					while (mTransferEncoding == TE_Chunked_header)
					{										
						while (receiveBuffer + *sequenceLength != requestLine && ((*requestLine >= '0' && *requestLine <= '9') || (((*requestLine)&~32) >= 'A' && ((*requestLine)&~32) <= 'F')))
						{
							mContentLength *= 16;
							mContentLength += (*requestLine >= '0' && *requestLine <= '9')? *requestLine - '0':
								((*requestLine)&~32) - 'A' + 10;
							requestLine ++;
						}
						if (receiveBuffer + *sequenceLength != requestLine && mContentLength != 0)
							mContentLength += 2;
						while (receiveBuffer + *sequenceLength != requestLine && (requestLine[-2] != '\r' || requestLine [-1] != '\n'))
							requestLine ++;
						if (receiveBuffer + *sequenceLength != requestLine)
							if (mContentLength > 0)
								mTransferEncoding = TE_Chunked_body;
							else
							{
								mTransferEncoding = TE_Chunked_trailer;
								break;
							}
						if (mTransferEncoding == TE_Chunked_body)
							if (*sequenceLength - mContentLength >= requestLine - receiveBuffer)
							{							
								mTransferEncoding = TE_Chunked_header;
								requestLine += mContentLength;
								mContentLength = 0;
							}
							else
							{
								mContentLength -= *sequenceLength + receiveBuffer - requestLine;
							}					
						if (*sequenceLength - mContentLength == requestLine - receiveBuffer)
							break;
					}

					while(mTransferEncoding == TE_Chunked_trailer)
					{
						mContentLength = 2;
						while (receiveBuffer + *sequenceLength != requestLine + mContentLength && (requestLine[mContentLength-2] != '\r' || requestLine [mContentLength-1] != '\n'))
							mContentLength ++;						
						if (mContentLength == 2)
						{
							mTransferEncoding = TE_Normal;
							mContentLength = 0;
							mQConditionEmptyLock.lock();
							mQConditionEmptyLock.unlock();
							mBufferEmptyVariable.notify_all();							
						}
						else
							requestLine += mContentLength;
					}
				}
			}
			mBufferTransferLock.lock();
			if (sequenceLength == 0)
				if(mIn.isConnected())
					continue;
				else
					if (mQueueCondition == QC_Empty)
					{
						mBufferTransferLock.unlock();
						goto endThread;
					}
			mQEnd = (mQEnd + 1) % mQueueSize;
			if (mQueueCondition == QC_Empty)
			{
				mQConditionEmptyLock.lock();
				mQConditionEmptyLock.unlock();
				mBufferEmptyVariable.notify_all();
			}
			if (mQEnd == mQBegin)
				mQueueCondition = QC_Full;
			else
				mQueueCondition = QC_Normal;
			
			if (mQueueCondition == QC_Full)
			{
				while (this->mQueueCondition == QC_Full && mOut.isConnected())
				{
					mBufferTransferLock.unlock();
					mBufferFullVariable.wait(conditionLock);
					mBufferTransferLock.lock();
				}

				if (!mOut.isConnected())
				{
					if (mProxyMode == PM_http)
					{
						if (mContentLength > 0 || this->mQueueCondition != QC_Empty)
						{													
							if (mPartner)
								mPartner->finishPacketTransmission();	
							mIn << "HTTP/1.1 502 Bad Gateway\r\n\r\n";
							writeLog(LT_Partner_Out, "HTTP/1.1 502 Bad Gateway\r\n\r\n");
							if (mPartner)
								mPartner->resumePacketTransmission();
							mQBegin = 0;
							mQEnd = 1;
							mQueueCondition = QC_Normal;
							mContentLength = 0;
							mBufferEmptyVariable.notify_all();
							continue;
						}
					}
					else
					{
						mIn.disconnect();
						goto endThread;
					}
				}
				mBufferTransferLock.unlock();
			}
			else
				mBufferTransferLock.unlock();		
		}
	}
endThread:	
	mBufferEmptyVariable.notify_all();			
}

void HTTPRelay::HTTPRelayWorker::backgroundSender()
{	
	unique_lock<mutex> conditionLock(mQConditionEmptyLock);
	char* sendBuffer;
	int* sequenceLength;
	int sent;	
	mBufferTransferLock.lock();
	while (true)//mOut.isConnected())
	{			
		if (mQueueCondition == QC_Empty)
		{						
			while (this->mQueueCondition == QC_Empty && mIn.isConnected())
			{
				mBufferTransferLock.unlock();
				mBufferEmptyVariable.wait(conditionLock);
				mBufferTransferLock.lock();
			}
			if (this->mQueueCondition == QC_Empty)
			{
				mBufferTransferLock.unlock();
				break;
			}			
		}		
		sendBuffer = mSharedBuffer + mQBegin * (mBlockSize) + mQueueSize * 4;
		sequenceLength = reinterpret_cast<int*>(mSharedBuffer + mQBegin * 4);
		mBufferTransferLock.unlock();								
		sent = 0;		
		while (sent < *sequenceLength)
		{
			sent += mOut.send(sendBuffer + sent, *sequenceLength - sent);		
			if (!mOut.isConnected())
			{
				mBufferFullVariable.notify_all();
				break;
			}
		}		
		writeLog(LT_Out,sendBuffer,*sequenceLength);
		mBufferTransferLock.lock();		
		mQBegin = (mQBegin + 1) % mQueueSize;					
		if (mQueueCondition == QC_Full)
		{
			mQConditionFullLock.lock();
			mQConditionFullLock.unlock();
			mBufferFullVariable.notify_all();
		}
		if (mQBegin == mQEnd)			
			mQueueCondition = QC_Empty;
		else
			mQueueCondition = QC_Normal;
	}
	mBufferTransferLock.lock();
	if (mIn.isConnected() == false || mProxyMode == PM_https)
	{		
		conditionLock.release();		
		delete this;		
	}	
}

void HTTPRelay::HTTPRelayWorker::finishPacketTransmission()
{	
	unique_lock<mutex> conditionLock(mQConditionEmptyLock);
	while(mContentLength > 0 || mTransferEncoding != TE_Normal)
	{
		mBufferEmptyVariable.wait(conditionLock);
	}
	mBufferTransferLock.lock();	
	/*unique_lock<recursive_mutex> conditionLock(mQConditionFullLock);
	while(true)
	{
		mBufferTransferLock.lock();
		if(mQueueCondition != QC_Empty && mOut.isConnected())
		{			
				mBufferTransferLock.unlock();			
				mBufferFullVariable.wait(conditionLock);
		}
		else
			break;
	}*/
}

void HTTPRelay::HTTPRelayWorker::resumePacketTransmission()
{
	mBufferTransferLock.unlock();
}

#define __LOCAL_COMPARE_STRINGS(X,Y) while(*(X) && (*(X++)&~32)==(*(Y++)&~32))
void HTTPRelay::HTTPRelayWorker::HTTPParseHeader(const char* pHeader)
{		
	const char*a,*b;
	a = "Host:";
	b = pHeader;
	__LOCAL_COMPARE_STRINGS(a,b);
	if (*b==' ')
	{		
		b++;
		a = b;
		int hostPort = 0;
		while (*a != 0 && *a != ':' && *a != ' ' && *a != '\r') a++;
		if (*a == ':')
		{
			const char *c=a+1;
			while(*c>='0' && *c<='9'){hostPort*=10;hostPort +=*(c++)-'0';};			
		}
		else
			hostPort = 80;				
		hostResolved = true;
		if (mActiveHostPort == hostPort && string(b,a-b) == mActiveHost)
			return;		
		if (mPartner)
			mPartner->finishPacketTransmission();						
		if (mOut.isConnected())
		{
			mOut.disconnect();
#if LOGLEVEL > 0
			cout << mWorkerID << " disconnected from " << mActiveHost<<":"<<mActiveHostPort << "\n";
			cout.flush();
#endif
		}
		mActiveHost.assign(b,a-b);
		mActiveHostPort = hostPort;
		ClientSocketStream s;
		if (s.connectToHost(mActiveHost.c_str(),hostPort))
		{		
#if LOGLEVEL > 0
			cout << mWorkerID << " connected to " << mActiveHost<<":"<<mActiveHostPort << "\n";
			cout.flush();
#endif
			mOut = s;
			if (mPartner == nullptr)			
			{
				mPartner = new HTTPRelayWorker(mOut,mIn,mBlockSize,mQueueSize,PM_http_response);																		
				mPartner->mPartner = this;
#if LOGLEVEL > 0
				mPartner->setWorkerID(mWorkerID + 1);
#endif
			}
			else
			{
				mPartner->mIn = s;
				mPartner->resumePacketTransmission();		
			}
			if (mPartner->mReceiverThread == nullptr)
			{
				mPartner->mReceiverThread = new thread([this](){this->mPartner->backgroundReceiver();});
				thread([this](){this->mPartner->backgroundSender();}).detach();				
			}			
		}
		else
		{			
			mIn << "HTTP/1.1 503 Service Unavailable\r\n\r\n";
			writeLog(LT_Partner_Out,"HTTP/1.1 503 Service Unavailable\r\n\r\n");
		}
		return;
	}

	a = "Content-Length:";
	b = pHeader;
	__LOCAL_COMPARE_STRINGS(a,b);
	if (*b==' ')
	{		
		b++;		
		a = b;
		while(*a==' ' || *a == '\t') a++;
		while(*a>='0' && *a<='9')
		{
			mContentLength*=10;
			mContentLength +=*(a++)-'0';
		}
	}
	
	a = "Transfer-Encoding:";
	b = pHeader;
	__LOCAL_COMPARE_STRINGS(a,b);
	if (*b==' ')
	{	
		do 
		{
			b++;	
			a = "chunked";
			__LOCAL_COMPARE_STRINGS(a,b);
			if (*a == 0)
				mTransferEncoding = TE_Chunked_header;
		}	while (*b != '\n' && *a != 0);	
	}
}

void HTTPRelay::HTTPRelayWorker::HTTPParseRequest(const char* pHeader)
{	
	const char*a,*b;
	a = "CONNECT";
	b = pHeader;
	__LOCAL_COMPARE_STRINGS(a,b);
	if (*b==' ')
	{		
		b++;		
		a = b;		
		int hostPort = 0;
		while (*a != 0 && *a != ':' && *a != ' ') a++;
		if (*a == ':')
		{
			const char *c=a+1;
			while(*c>='0' && *c<='9'){hostPort*=10;hostPort+=*(c++)-'0';};			
		}
		else
			hostPort = 80;		
		if (mPartner)
			mPartner->finishPacketTransmission();				
		if (mOut.isConnected())
		{
			mOut.disconnect();
#if LOGLEVEL > 0
			cout << mWorkerID << " disconnected from " << mActiveHost<<":"<<mActiveHostPort << "\n";
			cout.flush();
#endif
		}
		
		mActiveHost.assign(b,a-b);		
		mActiveHostPort = hostPort;
		ClientSocketStream s;
		if (!s.connectToHost(mActiveHost.c_str(),mActiveHostPort))					
		{
			mIn << "HTTP/1.1 503 Service Unavailable\r\n\r\n";	

			writeLog(LT_Partner_Out,"HTTP/1.1 503 Service Unavailable\r\n\r\n");			
			return;
		}
#if LOGLEVEL > 0
			cout << mWorkerID << " connected to " << mActiveHost<<":"<<mActiveHostPort << "\n";
			cout.flush();
#endif
		mOut = s;		
		bool hadPartner = mPartner != nullptr;
		mProxyMode = PM_https;						

		if (mPartner)		
		{
			mPartner->mIn = s;
			mPartner->resumePacketTransmission();
		}
		else
		{
			mPartner = new HTTPRelayWorker(s,mIn,mBlockSize,mQueueSize,PM_https);		
			mPartner->mPartner = this;
#if LOGLEVEL > 0
			mPartner->setWorkerID (mWorkerID + 1);
#endif
		}
		
		{
			mIn << "HTTP/1.1 200 Connection established\r\n\r\n";

			writeLog(LT_Partner_Out, "HTTP/1.1 200 Connection established\r\n\r\n");
		}
		if (mPartner->mReceiverThread == nullptr)
		{
			mPartner->mReceiverThread = new thread([this](){this->mPartner->backgroundReceiver();});
			thread([this](){this->mPartner->backgroundSender();}).detach();
		}		
	}

}
