#pragma once
#include "Buffer.h"
class BaseJob
{
public:
	virtual void operator()(Buffer* pBuffer);
};

