#include "stdafx.h"
#include "userinfo.h"
#include <fstream>
UserInfo::UserInfo(std::string u, std::string p) : usage (0), username(u), password(p), shouldEnd(false)
{
	std::thread tmp ([this](){this->printFunc();});
	logThread.swap(tmp);
}

UserInfo::UserInfo(UserInfo &&other) : 
	usage (other.usage), 
	shouldEnd (other.shouldEnd),
	username(std::move(other.username)), 
	password(std::move(other.password))
{
	other.shouldEnd = true;
	other.logThread.join();
	std::thread tmp ([this](){this->printFunc();});
	logThread.swap(tmp);
}

UserInfo::~UserInfo()
{
	this->shouldEnd = true;
	this->logThreadNotification.notify_all();
	if(logThread.joinable())
		logThread.join();
}

void UserInfo::printFunc()
{
	std::unique_lock<std::mutex> lock(logThreadMutex);
	std::ofstream log((getUserName() + ".log").c_str());
	while(!this->shouldEnd)
	{			
		log.seekp(0, std::ios::beg);
		char header[80];			
		for (int k = sprintf(header, "usage : %d%s(%d bytes)", usage>1024?usage/1024>1024?usage/1024/1024:usage/1024:usage,usage>1024?usage/1024>1024?"MB":"KB":"B",usage);k<sizeof(header) - 1;k++)
			header[k] = ' ';
		header[sizeof(header) - 1] = '\n';
		log.write(header, 80);
		log.seekp(0, std::ios::end);
		log.flush();			
		this->logThreadNotification.wait_for(lock, std::chrono::seconds(60*15));
	}
	log.close();
}

void UserInfo::addTraffic(int bytes)
{
	std::lock_guard<std::mutex> lock(logThreadMutex);
	usage += bytes;
}

const std::string UserInfo::getUserName()
{
	return username;
}

const std::string UserInfo::getPassWord()
{
	return password;
}