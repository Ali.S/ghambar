#pragma once
#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>

struct UserInfo
{
	UserInfo(std::string username, std::string password);
	UserInfo (UserInfo &&other);
	~UserInfo();
	void addTraffic(int bytes);
	const std::string getUserName();
	const std::string getPassWord();
private:
	UserInfo &operator = (const UserInfo& other){};
	UserInfo (UserInfo &other){};	
	void printFunc();
	int usage;	
	std::string username;
	std::string password;
	std::thread logThread;
	std::mutex logThreadMutex;
	std::condition_variable logThreadNotification;
	bool shouldEnd;
};