#pragma once
#include "Configuration.h"
#include "SocketStream.h"
#include <mutex>
#include <condition_variable>
#include <thread>
#include <fstream>

class HTTPRelay
{	
public: 
	enum ProxyMode
	{
		PM_http,
		PM_http_request,
		PM_http_response,
		PM_https
	};

	HTTPRelay(SocketStream &A, SocketStream &B, int BlockSize, int queueSize, ProxyMode mode);
#if LOGLEVEL > 0
	static std::string appname;
	static int conID;	
#endif
private:	
	struct HTTPRelayWorker{
#if LOGLEVEL > 0
		std::ofstream LOGIn;
		std::ofstream LOGOut;
		int mWorkerID;		
		void setWorkerID(int ID);
		enum LOGType
		{
			LT_In,
			LT_Out,
			LT_Partner_In,
			LT_Partner_Out
		};
		void writeLog(LOGType type, const char* data, int length);		
		template <typename T> void writeLog(LOGType type, T data)
		{
			std::stringstream s;
			s << data << "\n";
			writeLog(type,s.str().c_str(),s.str().size());
		}
#ifdef WIN32
		void writeTime(LOGType type);
		unsigned long long CPUStartClock;
		unsigned long long CPUPerformance;
#else
#define writeTime(X)
#endif
#else
#define writeLog(X, ...)
#define writeTime(X)
#endif
		HTTPRelayWorker(SocketStream &in, SocketStream &out, int BlockSize, int queueSize, ProxyMode mode);	
		~HTTPRelayWorker();
		enum QueueCondition
		{
			QC_Empty,
			QC_Full,
			QC_Normal,
			QC_Done
		} mQueueCondition;
		enum TransferEncoding
		{
			TE_Normal,
			TE_Chunked_header,
			TE_Chunked_body,
			TE_Chunked_trailer,
		} mTransferEncoding;
		const int mBlockSize;
		const int mQueueSize;
		int mQBegin,mQEnd,mContentLength;
		char *mSharedBuffer;	
		SocketStream mIn;
		SocketStream mOut;
		ProxyMode mProxyMode;
		std::mutex mBufferTransferLock;
		std::condition_variable_any mBufferFullVariable;
		std::condition_variable mBufferEmptyVariable;
		std::recursive_mutex mQConditionFullLock;	
		std::mutex mQConditionEmptyLock;	
		std::thread* mReceiverThread;				
		HTTPRelayWorker* mPartner;
		std::string mActiveHost;		
		bool hostResolved;
		int mActiveHostPort;
		void backgroundReceiver();
		void backgroundSender();
		void finishPacketTransmission();
		void resumePacketTransmission();
		void HTTPParseHeader(const char* pHeader);
		void HTTPParseRequest(const char* pHeader);					
	};	
};

#if LOGLEVEL > 0
template <> void HTTPRelay::HTTPRelayWorker::writeLog(HTTPRelay::HTTPRelayWorker::LOGType type, const char* data);
#endif