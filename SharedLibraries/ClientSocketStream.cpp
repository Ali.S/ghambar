#include "stdafx.h"
#include "ClientSocketStream.h"
#include <memory.h>
#ifndef SOCKET_ERROR
#define SOCKET_ERROR -1
#endif

ClientSocketStream::ClientSocketStream() : SocketStream()
{

}

ClientSocketStream::ClientSocketStream(const char* pServerIP, int pServerPort) : SocketStream()
{
	connect (pServerIP, pServerPort);
}

ClientSocketStream::~ClientSocketStream()
{
	
}

bool ClientSocketStream::connectToHost(const char *pServerName, int pServerPort)
{	
	hostent* ent = gethostbyname(pServerName);
	if (ent)
	{
		
		bool res = connect(*(int*)ent->h_addr_list[0], pServerPort);
		return res;
	}	
	return false;
}

bool ClientSocketStream::connect(const char *pServerIP,int pServerPort)
{	
	return connect(inet_addr(pServerIP),pServerPort);
}

bool ClientSocketStream::connect(unsigned long pServer,int pServerPort)
{
	initialize();
	if (mConnected)
		disconnect(); // Disconnect from connection if the class is Still Connected
	sockaddr_in socketAddress;
	memset( &socketAddress, 0, sizeof(socketAddress) ); // Reset connection informaion

	socketAddress.sin_family = AF_INET; // Set Connection type to IP4 version
	socketAddress.sin_addr.s_addr = pServer; // Generation an int host address based on ip provided And set IP address
	socketAddress.sin_port = htons( pServerPort); // Convert port provided to TCP/IP byte order (big indian) and set port
	if ( ::connect( mSocket, (sockaddr*)(&socketAddress), sizeof(socketAddress) ) == SOCKET_ERROR ) // Try to connect to the server with ip and port provided.
	{
#ifdef WIN32
		int error =WSAGetLastError();
#endif
		return false; // Report function failure
	}
	else
	{
		mConnected = CM_Both; // Mark the socket is connected
		return true;  // Report function success
	}
}

