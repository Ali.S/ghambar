#pragma once
#define LOG_NONE LOG_STATUS
#define LOG_STATUS 1
#define LOG_PACKAGES 2
#define LOG_ALL 3
#define LOGLEVEL LOG_NONE
#define BLOCKSIZE 4
#define QUEUESIZE 16*1024