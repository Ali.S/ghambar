#ifndef _SERVERSOCKETSTREAM_
#define _SERVERSOCKETSTREAM_

#include "SocketStream.h"

class ServerSocketStream : public SocketStream
{
public :
	ServerSocketStream ();
	ServerSocketStream (int pServerPort);
	~ServerSocketStream();
	bool bind (int pServerPort);
	SocketStream checkForNewConnection();
	SocketStream waitForNewConnection();
	sockaddr_in lastconaddr;
	socklen_t lastconaddrlen;
};

#endif