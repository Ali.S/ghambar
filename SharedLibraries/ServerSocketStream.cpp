#include "stdafx.h"
#include "ServerSocketStream.h"
#include <memory.h>

#ifndef SOCKET_ERROR
#define SOCKET_ERROR -1
#endif


ServerSocketStream::ServerSocketStream() : SocketStream()
{

}

ServerSocketStream::ServerSocketStream(int pServerPort) : SocketStream()
{
	bind(pServerPort);
}

ServerSocketStream::~ServerSocketStream()
{
#ifdef WIN32
	mInstancesCreated --; // Decrease The Number of instances Created from this class
	if (mInstancesCreated == 0) // Check if this class is the last class using Socket
		WSACleanup(); // Clean up dll loaded Files
#endif
}

bool ServerSocketStream::bind(int pServerPort)
{
	initialize();
	sockaddr_in socketAddress;
	memset( &socketAddress, 0, sizeof(socketAddress) ); // Reset connection informaion

	socketAddress.sin_family = AF_INET;					// Set Connection type to IP4 version
	socketAddress.sin_addr.s_addr = INADDR_ANY;	// Set ip for server (not important)
	socketAddress.sin_port = htons( pServerPort);		// Convert port provided to TCP/IP byte order (big indian) and set port
	
	char yes = 1;
	setsockopt(mSocket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));
	
	if ( ::bind( mSocket, (sockaddr*)(&socketAddress), sizeof(socketAddress) ) == SOCKET_ERROR )	// try to bind ip address return SOCKET_ERROR on fail
		return false;
	else
		if (listen(mSocket, SOMAXCONN) == SOCKET_ERROR) // Set Socket to Listening mode
			return false;
		else
			return true;
}

SocketStream ServerSocketStream::checkForNewConnection()
{
	fd_set set;
	FD_ZERO(&set);
	FD_ISSET(mSocket, &set);
	timeval time;
	time.tv_sec = time.tv_usec = 0;
	lastconaddrlen = sizeof(lastconaddr);
	memset(&lastconaddr,0,lastconaddrlen);
	if (select(mSocket + 1,&set,NULL,NULL,&time))						// Check if there is any change in connection
		return SocketStream(accept (mSocket, (sockaddr*)&lastconaddr, &lastconaddrlen));	// There is someone waiting to connect so we accept him and then create a SocketStream from it.
	else
		return SocketStream();
}

SocketStream ServerSocketStream::waitForNewConnection()
{
	fd_set set;
	FD_ZERO(&set);
	FD_ISSET(mSocket, &set);
	lastconaddrlen = sizeof(lastconaddr);
	memset(&lastconaddr,0,lastconaddrlen);
	return SocketStream(accept (mSocket, (sockaddr*)&lastconaddr, &lastconaddrlen));	// There is someone waiting to connect so we accept him and then create a SocketStream from it.	
}