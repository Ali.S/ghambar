/*
 *  sha1.h
 *
 *  Description:
 *      This is the header file for code which implements the Secure
 *      Hashing Algorithm 1 as defined in FIPS PUB 180-1 published
 *      April 17, 1995.
 *
 *      Many of the variable names in this code, especially the
 *      single character names, were used because those were the names
 *      used in the publication.
 *
 *      Please read the file sha1.c for more information.
 *
 */
#pragma once
#ifndef _SHA1_H_
#define _SHA1_H_

/*
 * If you do not have the ISO standard stdint.h header file, then you
 * must typdef the following:
 *    name              meaning
 *  uint32_t         unsigned 32 bit integer
 *  uint8_t          unsigned 8 bit integer (i.e., unsigned char)
 *  int_least16_t    integer of >= 16 bits
 *
 */

#define SHA1HashSize 20
#include <cstdint>
/*
 *  This structure will hold context information for the SHA-1
 *  hashing operation
 */
class SHA1
{	
public:
	enum class Error
	{
		Success = 0,
		Null,            /* Null pointer parameter */
		InputTooLong,    /* input data too long */
		StateError       /* called Input after Result */
	};
	SHA1();
	Error reset();
	Error input(const uint8_t *data, unsigned int length);
	Error result(uint8_t Message_Digest[SHA1HashSize]);
	
private:
	void PadMessage();
	void ProcessMessageBlock();
    uint32_t Intermediate_Hash[SHA1HashSize/4]; /* Message Digest  */

    uint32_t Length_Low;            /* Message length in bits      */
    uint32_t Length_High;           /* Message length in bits      */

                               /* Index into message block array   */
    int_least16_t Message_Block_Index;
    uint8_t Message_Block[64];      /* 512-bit message blocks      */

    bool Computed;               /* Is the digest computed?         */
    Error Corrupted;             /* Is the message digest corrupted? */
	
} ;

#endif