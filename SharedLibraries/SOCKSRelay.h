#pragma once
#include "Configuration.h"
#include "SocketStream.h"
#include <mutex>
#include <condition_variable>
#include <thread>
#include <fstream>
#include <atomic>
#include "userinfo.h"
#include <memory>

class SOCKSRelay
{	
public: 
	enum ProxyMode
	{
		PM_socks_uninitilized,
		PM_socks_initilized
	};
	
#if LOGLEVEL >= LOG_STATUS
	SOCKSRelay(SocketStream &A, SocketStream &B, int queueSize, ProxyMode mode, int conID, std::shared_ptr<UserInfo> user = nullptr);
	static std::string appname;
	static std::atomic<int> activeWorkers;
	static std::atomic<int> connectionSpeedUpload;
	static std::atomic<int> connectionSpeedDownload;
#else
	SOCKSRelay(SocketStream &A, SocketStream &B, int queueSize, ProxyMode mode, std::shared_ptr<UserInfo> user = nullptr);
#endif

private:	
	struct SOCKSRelayWorker{
#if LOGLEVEL >= LOG_STATUS
		std::ofstream LOGIn;
		std::ofstream LOGOut;
		int mWorkerID;		
		void setWorkerID(int ID);
		enum LOGType
		{
			LT_In,
			LT_Out,
			LT_Partner_In,
			LT_Partner_Out
		};
		void writeLog(LOGType type, const char* data, int length);		
		template <typename T> void writeLog(LOGType type, T data)
		{
			std::stringstream s;
			s << data << "\n";
			writeLog(type,s.str().c_str(),s.str().size());
		}		
		std::string mActiveHost;
		bool hostResolved;
		int mActiveHostPort;				
#ifdef WIN32
		void writeTime(LOGType type);
		unsigned long long CPUStartClock;
		unsigned long long CPUPerformance;
#else
#define writeTime(X)
#endif
#else
#define writeLog(X, ...)
#define writeTime(X)
#endif
		SOCKSRelayWorker(SocketStream &in, SocketStream &out, int queueSize, ProxyMode mode, std::shared_ptr<UserInfo> user);	
		~SOCKSRelayWorker();
		enum QueueCondition
		{
			QC_Empty,
			QC_Full,
			QC_Normal,
			QC_Done
		};
		const int mQueueSize;
		char *mBuffer;
		SOCKSRelayWorker* mPartner;
		SocketStream mIn;
		SocketStream mOut;
		ProxyMode mProxyMode;
		void workerMain();
		bool parseHeader();
		bool parseV4Header(int32_t& packetLength, int32_t& matchedLength);
		bool parseV4Header_FindString(int32_t& packetLength, int32_t& matchedLength);
		std::shared_ptr<UserInfo> mUser;
	};	
};

#if LOGLEVEL >= LOG_STATUS
template <> void SOCKSRelay::SOCKSRelayWorker::writeLog(SOCKSRelay::SOCKSRelayWorker::LOGType type, const char* data);
#endif