// Server.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <chrono>
#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#ifndef WIN32
#include <signal.h>
#endif
#include <vector>
#include <map>
using namespace std;

std::map<std::string, shared_ptr<UserInfo>> users;

void newConnection(SocketStream A, int conID)
{
	try{
	A.SetProtocolMode(static_cast<SocketStream::ProtocolMode>(SocketStream::PM_DISGUISE_SERVER | SocketStream::PM_Encrypted));	
	/*int userLength; 
	if (A.receive((char*)&userLength, 4) != 4)
		return;
	string userName;	
	cerr <<userLength <<"\n";
	userName.resize(userLength);	
	A.receive(const_cast<char*>(userName.data()), userLength);
	auto user = users.find(userName);
	if (user == users.end())
		return;
	string pass = user->second->getPassWord();	
	const int randLength = 50;
	for(int i=0;i<50;i++)
		pass.push_back('a' + (rand() % 26));
	A.send((char*)&randLength, 4);
	A.send(pass.data() + user->second->getPassWord().length(), randLength);	
	uint8_t clientPass[SHA1HashSize + 5], hash[SHA1HashSize + 5];
	A.receive((char*)clientPass, SHA1HashSize);
	SHA1 sha;	
	sha.input((uint8_t*)(pass.data()), pass.size());
	sha.result(hash);
	for(int i=0;i<SHA1HashSize;i++)
		if (clientPass[i] != hash[i])
			return;*/
	auto user = users.find("ali");
	SocketStream temp;
	if (A.isConnected())	
	{
#if LOGLEVEL > 0		
		SOCKSRelay(A,temp,QUEUESIZE,SOCKSRelay::PM_socks_uninitilized,conID, user->second);		
#else
		SOCKSRelay(A,temp,QUEUESIZE,SOCKSRelay::PM_socks_uninitilized, user->second);		
#endif		
	}}
	catch(std::exception e)
	{
		cerr << e.what() << "\n";
	}
	catch(...)
	{
		cerr << "some bad error";
	}
}

int main(int argc, char* argv[])
{	
#if LOGLEVEL > 0
	system("rm -f ../Log/*.txt");
#endif	
	ifstream userlist("users.list");
	string username,password;
	while (userlist >> username >> password)
	{
		for(int i=0;i<username.length();i++)
			username[i] = tolower(username[i]);
		users.insert(std::make_pair(username,std::make_shared<UserInfo>(username, password)));		
	}
	ServerSocketStream server;
	if(!server.bind(80))
	{
		cout << "port is already in use\n";
		return 1;
	}
	server.SetBlockMode(SocketStream::BM_ALWAYS);	
	int threadID = 0;	
	SocketStream A;
	
	std::atomic<int> conID(0);	
#ifndef WIN32
	signal(SIGPIPE,SIG_IGN);
#endif

#if LOGLEVEL > 0
	thread ([&conID](){
		while(true)
		{			
			int a = conID;
			int b = SOCKSRelay::activeWorkers;
			printf("last Connection : %6d, workers : %4d\n",a,b);
			cout.flush();
			this_thread::sleep_for(chrono::milliseconds(500));
		}
	}).detach();
	SOCKSRelay::appname = "ser";
#endif	

	while (true)
	{			
		SocketStream A=server.waitForNewConnection();		
		thread(newConnection,A,conID++).detach();
	}
	return 0;
}

