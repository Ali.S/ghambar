#include "stdafx.h"
#include <thread>
#include <string>
#include <fstream>
using namespace std;

string username,password;

void newConnection(SocketStream A,int conID)
{
	string s;
	ClientSocketStream B;
	//B.connectToHost("minecraft.legeria.com",8008);
	//B.connectToHost("127.0.0.1",80);
	//B.connectToHost("208.68.36.50",8008);
	//B.connectToHost("172.16.53.200",8008);
	//B.connectToHost("82.196.2.52",8008);
	B.connectToHost("178.62.239.197",81);
	B.SetProtocolMode(static_cast<SocketStream::ProtocolMode>(SocketStream::PM_DISGUISE_CLIENT | SocketStream::PM_Encrypted));	
	/*int userlength = username.length();
	B.send((char*)&userlength, 4);
	B.send(username.data(), username.length());
	string randKey = password;
	int randLength = 0;
	B.receive((char*)&randLength, 4);
	randKey.resize(randKey.size() + randLength);
	B.receive((char*)randKey.data() + password.length(), randLength);
	uint8_t hash[SHA1HashSize];
	SHA1 sha;
	sha.input((uint8_t*) randKey.data(), randKey.length());
	sha.result(hash);
	B.send((char*)hash,SHA1HashSize);*/
	if (B.isConnected())
	{
		//std::cout<< conID  << ": Connection ID " << "\n";
#if LOGLEVEL > 0
		SOCKSRelay(A,B,QUEUESIZE,SOCKSRelay::PM_socks_initilized, conID);
#else
		SOCKSRelay(A,B,QUEUESIZE,SOCKSRelay::PM_socks_initilized);
#endif
		//A >> s;
		//B << s;		
		
	}
	else
	{
		std::cout<< conID  << ": Connection failed " << conID << "\n";
	}
}

int main(int argc, char* argv[])
{
#if LOGLEVEL > 0
	system("rm -f ../Log/*.txt");
#endif

	//cout << "what is a your UserName?\n";	
	//cin >> username;	
	//for(int i=0;i<username.length();i++)
//		username[i] = tolower(username[i]);
	//cout << "and your password?\n";
	//cin >> password;
	ServerSocketStream server;	
	if (server.bind(8123) == false)
	{
		cout << "port is in use\n";
		return 0;
	}
	server.SetBlockMode(SocketStream::BM_ALWAYS);	
	int conID = 0;	
	SocketStream A;
#if LOGLEVEL > 0
	thread ([](){
		int reportSpeedUp[] = {0,0,0,0,0,0,0,0,0,0};
		int reportSpeedDown[] = {0,0,0,0,0,0,0,0,0,0};
			
		while(true)
		{
			char scope[][10] = {"B", "KB", "MB", "GB"};
			//cout << "workers : " << SOCKSRelay::activeWorkers;
			int scopeIDUp = 0;
			for (int i=1;i<sizeof(reportSpeedUp) / sizeof (reportSpeedUp[0]);i++)
				reportSpeedUp[i-1] = reportSpeedUp[i];
			for (int i=1;i<sizeof(reportSpeedDown) / sizeof (reportSpeedDown[0]);i++)
				reportSpeedDown[i-1] = reportSpeedDown[i];
			reportSpeedUp[sizeof(reportSpeedUp) / sizeof (reportSpeedUp[0]) - 1] = SOCKSRelay::connectionSpeedUpload;
			reportSpeedDown[sizeof(reportSpeedDown) / sizeof (reportSpeedDown[0]) - 1] = SOCKSRelay::connectionSpeedDownload;
			int avup = 0, avdown = 0;
			for (int i=0;i<sizeof(reportSpeedUp) / sizeof (reportSpeedUp[0]);i++)
				avup += reportSpeedUp[i];
			for (int i=0;i<sizeof(reportSpeedDown) / sizeof (reportSpeedDown[0]);i++)
				avdown += reportSpeedDown[i];
			avup /= 5;
			avdown /= 5;
			while (avup> 1024 * 4 && scopeIDUp < 4)
			{
				scopeIDUp ++;
				avup /= 1024;
			}
			//cout << ", upload : " << (connectionSpeedUpload) << scope[scopeID]; 
			int scopeIDDown = 0;
			while (avdown > 1024 * 4 && scopeIDDown < 4)
			{
				scopeIDDown ++;
				avdown /= 1024;
			}
			int worker = SOCKSRelay::activeWorkers;
			//cout << ", upload : " << (connectionSpeedDownload) << scope[scopeID]; 
			//cout << "\n";
			printf("workers: %5d, upload: %*d%s, download: %*d%s\n",
				worker,scopeIDUp==0?5:4,avup,scope[scopeIDUp],scopeIDDown==0?5:4,avdown, scope[scopeIDDown]);
			SOCKSRelay::connectionSpeedUpload = 0;
			SOCKSRelay::connectionSpeedDownload = 0;
			cout.flush();
			this_thread::sleep_for(chrono::milliseconds(500));
		}
	}).detach();
	SOCKSRelay::appname = "cli";
#endif	
	while (true)
	{		
		A = server.waitForNewConnection();
		thread(newConnection,A,conID++).detach();
	}
	return 0;
}

