// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include "../SharedLibraries/ClientSocketStream.h"
#include "../SharedLibraries/ServerSocketStream.h"
#include "../SharedLibraries/HTTPRelay.h"
#include "../SharedLibraries/SOCKSRelay.h"
#include "../SharedLibraries/SHA1.h"

// TODO: reference additional headers your program requires here
