#include "stdafx.h"
#include <iostream>

using namespace std;

int main()
{
	SOCKSRelay::appname = "pptpcli";	
	ServerSocketStream serv;
	if (serv.bind(1723) == false)
		cout << "some error\n";
	else
		cout << "yay!\n";
	SocketStream con = serv.waitForNewConnection();
	ClientSocketStream target ("82.196.2.52",8009);	
	target.SetProtocolMode(static_cast<SocketStream::ProtocolMode>(SocketStream::PM_Encrypted | SocketStream::PM_DISGUISE_CLIENT));
	if (target.isConnected() == false)
		cout << "middle man unreachable!\n";
	else
		cout << "yay!\n";

	SOCKSRelay socks(con,target,QUEUESIZE,SOCKSRelay::PM_socks_initilized,0);
	system("pause");
	return 0;	
}