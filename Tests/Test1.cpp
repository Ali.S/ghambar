#include <SocketStream.h>
#include <ServerSocketStream.h>
#include <ClientSocketStream.h>
#include <thread>
#include <atomic>
#include <chrono>
#include <iostream>
#include <mutex>
#include <condition_variable>
#include <atomic>

#define packetSize 1000
#define connectionCount 1000
#define packetSendFrequency 0
#define packetReceiveFrequency 0

using namespace std;

mutex startGunMutex;
condition_variable startGun;

void sender(SocketStream s, atomic<long long> &counter)
{
	unique_lock<mutex> startGunLock(startGunMutex);
	startGun.wait(startGunLock);
	startGunLock.release();
	startGunMutex.unlock();
	char *data= new char[packetSize];
	counter = 0;
	while (s.isConnected())
	{
#if packetSendFrequency > 0
			this_thread::sleep_for(chrono::microseconds(1000000LL / packetSendFrequency));
#endif
		for(long long i= 0; i< packetSize;i++)
			data[i] = counter + i;
		s.send(data, packetSize);				
		counter++;
	}
	s.closeSocket();
	delete []data;
}

void receiver(SocketStream s,atomic<long long>& counter, atomic<long long>& lost)
{
	char *data = new char[packetSize];	
	bool packetlost = false;
	int par = 0;
	counter = 0;
	while (s.isConnected())
	{
#if packetReceiveFrequency > 0
			this_thread::sleep_for(chrono::microseconds(1000000LL / packetReceiveFrequency));
#endif
		if ((par += s.receive(data,packetSize - par)) < packetSize)
			continue;
		packetlost = false;
		for(long long i=0;i<packetSize;i++)
			if (data[i] != (char)(counter + i))
				packetlost = true;
		counter++;
		if (packetlost)
			lost ++;
		par = 0;
	}
	s.closeSocket();
	delete []data;
}

thread senders[connectionCount];
thread receivers[connectionCount];
atomic<long long> sent[connectionCount] = {0}, received[connectionCount] = {0}, lost [connectionCount]= {0};
ServerSocketStream servers[connectionCount];
ClientSocketStream clients[connectionCount];
int main()
{
	this_thread::sleep_for(chrono::seconds(1));
	chrono::high_resolution_clock::time_point start = chrono::high_resolution_clock::now();
	int portnum = 9000;
	cout << "inializing tests\n";
	for(int i=0;i<connectionCount;i++)
	{
		sent[i] = received[i] = lost[i] = 0;
		while(!servers[i].bind(portnum++)) cout << portnum << "\n";
		clients[i].connect("127.0.0.1",8081);
		if (!clients[i].isConnected())
		{
			cout << "Test " << i << " failed\n";
			continue;	
		}
		char *request = new char [10];
		request[0] = 4;
		request[1] = 1;
		*reinterpret_cast<int16_t*>(request+2) = portnum - 1;
		swap(request[2],request[3]);
		request[4] = 127;
		request[5] = 0;
		request[6] = 0;
		request[7] = 1;
		request[8] = 0;
		clients[i].send(request,9);
		//client << "CONNECT 127.0.0.1:9000 HTTP/1.1\r\n\r\n";
		SocketStream con = servers[i].waitForNewConnection();		
		string res;
		//client >> res;
		clients[i].SetBlockMode(SocketStream::BM_ALWAYS);
		clients[i].receive(request,8);
		if (request[1] == 'Z')
		{
			con.SetBlockMode(SocketStream::BM_ALWAYS);
			if (((i+1) * 10) % connectionCount == 0)
				cout << "test " << i + 1 << " began\n";			
			receivers[i].~thread();
			new(receivers+i)thread([&con,i]{receiver(con,received[i],lost[i]);});
			senders[i].~thread();
			new(senders+i)thread(thread([i]{sender(clients[i],sent[i]);}));			
		}
		else
			cout << "test " << i + 1 << "failed\n";
	}
	cout << "all tests started\n" << "\n";
	cout.flush();
	startGun.notify_all();
	cout << "all tests started\n" << "\n";
	cout.flush();
	cin.get();
	long long sumSent = 0, sumReceived = 0, sumLost = 0;
	for(int i=0;i<connectionCount;i++)
	{
		clients[i].disconnect();
		sumSent += sent[i];
		sumReceived += received[i];
		sumLost += lost[i];
	}
	cout << "finishing test so far :";
	cout << sumSent << "/"<< sumReceived << "/" << sumLost << "\n";				
	for(int i=0;i<connectionCount;i++)
	{
		cout.flush();
		if (senders[i].joinable())
			senders[i].join();
		cout.flush();
		if (receivers[i].joinable())
			receivers[i].join();
		cout.flush();
	}
	
	chrono::high_resolution_clock::time_point end = chrono::high_resolution_clock::now();
	cout << "test done\n";
	cout << "total time: " << chrono::duration_cast<chrono::milliseconds>(end - start).count() / 1000.f <<"\n";
	sumSent = 0;
	sumReceived = 0;
	sumLost = 0;
	long long maxLost = -1;
	long long maxLostSent = 0;
	long long maxLostReceived = 0;
	int maxLostID = 0;
	for(int i=0;i<connectionCount;i++)
	{
		sumSent += sent[i];
		sumReceived += received[i];
		lost[i] += sent[i] - received[i];
		sumLost += lost[i];		
		if (lost[i] > maxLost)
		{
			maxLostID = i;
			maxLost = lost[i];
			maxLostSent = sent[i];
			maxLostReceived = received[i];			
		}
	}	
	printf("packets sumSent: %lld(%.2f kp/s)\n", sumSent, sumSent * 1000.f / chrono::duration_cast<chrono::microseconds>(end - start).count());
	printf("data sumSent: %lldMB(%.2f MB/s)\n", sumSent * packetSize / 1024 / 1024, sumSent * packetSize / 1.024f / 1.024f / chrono::duration_cast<chrono::microseconds>(end - start).count());
	printf("packets sumReceived: %lld(%.2f kp/s)\n", sumReceived, sumReceived * 1000.f / chrono::duration_cast<chrono::microseconds>(end - start).count());
	printf("data sumReceived: %lldMB(%.2f MB/s)\n", sumReceived * packetSize / 1024 / 1024, sumReceived * packetSize / 1.024f / 1.024f / chrono::duration_cast<chrono::microseconds>(end - start).count());
	printf("packets sumLost: %lld (%%%.1f)\n",sumLost, sumLost * 100.f / (float)sumSent);
	printf("worst case:\n");
	printf("packets Sent: %lld(%.2f kp/s)\n", maxLostSent, maxLostSent * 1000.f / chrono::duration_cast<chrono::microseconds>(end - start).count());
	printf("data Sent: %lldMB(%.2f MB/s)\n", maxLostSent * packetSize / 1024 / 1024, maxLostSent * packetSize / 1.024f / 1.024f / chrono::duration_cast<chrono::microseconds>(end - start).count());
	printf("packets Received: %lld(%.2f kp/s)\n", maxLostReceived, maxLostReceived * 1000.f / chrono::duration_cast<chrono::microseconds>(end - start).count());
	printf("data Received: %lldMB(%.2f MB/s)\n", maxLostReceived * packetSize / 1024 / 1024, maxLostReceived * packetSize / 1.024f / 1.024f / chrono::duration_cast<chrono::microseconds>(end - start).count());
	printf("packets Lost: %lld (%%%.1f)\n",maxLost, maxLost * 100.f / (float)maxLostSent);
	cin.get();
}