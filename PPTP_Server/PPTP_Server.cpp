// Server.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <chrono>
#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#ifndef WIN32
#include <signal.h>
#endif
using namespace std;

void newConnection(SocketStream A, int conID)
{
	A.SetProtocolMode(static_cast<SocketStream::ProtocolMode>(SocketStream::PM_Encrypted | SocketStream::PM_DISGUISE_SERVER));	
	ClientSocketStream temp("208.68.36.50",1723);
	if (A.isConnected())	
	{
#if LOGLEVEL > 0		
		SOCKSRelay(A,temp,QUEUESIZE,SOCKSRelay::PM_socks_initilized,conID);		
#else
		SOCKSRelay(A,temp,QUEUESIZE,SOCKSRelay::PM_socks_initilized);		
#endif		
	}
}


int main(int argc, char* argv[])
{	
#if LOGLEVEL > 0
	system("rm -f ../Log/*.txt");
#endif
	ServerSocketStream server;
	if(!server.bind(8009))
	{
		cout << "port is already in use\n";
		return 1;
	}
	server.SetBlockMode(SocketStream::BM_ALWAYS);	
	int threadID = 0;	
	SocketStream A;
	
	std::atomic<int> conID(0);	
#ifndef WIN32
	signal(SIGPIPE,SIG_IGN);
#endif

#if LOGLEVEL > 0
	thread ([&conID](){
		while(true)
		{			
			int a = conID;
			int b = SOCKSRelay::activeWorkers;
			printf("last Connection : %6d, workers : %4d\n",a,b);
			cout.flush();
			this_thread::sleep_for(chrono::milliseconds(500));
		}
	}).detach();
	SOCKSRelay::appname = "pptpser";
#endif	

	while (true)
	{			
		SocketStream A=server.waitForNewConnection();		
		thread(newConnection,A,conID++).detach();
	}
	return 0;
}

